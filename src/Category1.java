import lombok.Data;

import java.io.Serializable;

@Data
public class Category1 implements Serializable {
    private String name;

    /**
     * Constructor for Category1
     * @param name Category1 name. String.
     */
    public Category1 (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "\"" + name + "\"";
    }
}
