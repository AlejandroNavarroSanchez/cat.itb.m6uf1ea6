import java.util.ArrayList;
import java.util.List;

public class ListaDepartaments {
    private List<Departament> lista = new ArrayList<Departament>();

    /**
     * Constructor for ListaDepartaments, creates a list of {@link Departament}
     */
    public ListaDepartaments(){
    }

    public void add(Departament d) {
        lista.add(d);
    }

    public List<Departament> getDeparmentList() {
        return lista;
    }
}
