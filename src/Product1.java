import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Product1 implements Serializable {
    private String name;
    private Long price;
    private Long stock;
    private String picture;
    private List<Category1> categories;

    /**
     * Constructor for Product1
     * @param name Name of the product. String
     * @param price Price of the product. Long
     * @param stock Stock of the product. Long
     * @param picture Picture of the product. String
     * @param categories Product categories. List<Category1>
     */
    public Product1(String name, Long price, Long stock, String picture, List<Category1> categories) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.picture = picture;
        this.categories = categories;
    }
}
