import java.io.Serializable;

public class Empleat implements Serializable {
    private int id;
    private String sureName;
    private int department;
    private Double salary;

    /**
     * Constructor for Empleat. Creates a new Empleat.
     * @param id Employee's ID
     * @param sureName Employee's surename
     * @param department Employee's department number
     * @param salary Employee's salary
     */
    public Empleat(int id, String sureName, int department, Double salary) {
        this.id = id;
        this.sureName = sureName;
        this.department = department;
        this.salary = salary;
    }
    // Getters
    public int getId() {
        return id;
    }
    public String getSureName() {
        return sureName;
    }
    public int getDepartment() {
        return department;
    }
    public Double getSalary() {
        return salary;
    }
    // Setters
    public void setId(int id) {
        this.id = id;
    }
    public void setSureName(String sureName) {
        this.sureName = sureName;
    }
    public void setDepartment(int department) {
        this.department = department;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return String.format("Empleat:\n\t→ ID: %d\n\t→ Cognom: %s\n\t→ Departament: %d\n\t→ Salari: %.02f", id, sureName, department, salary);
    }
}
