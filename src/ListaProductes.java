import java.util.ArrayList;
import java.util.List;

public class ListaProductes {
    private List<Product1> lista = new ArrayList<Product1>();

    /**
     * Constructor for ListaProductes, creates a list of {@link Product1}
     */
    public ListaProductes(){
    }

    public void add(Product1 p1) {
        lista.add(p1);
    }

    public List<Product1> getProductList() {
        return lista;
    }
}
