import com.thoughtworks.xstream.XStream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    // Format
    private static final String BOLD = "\033[1m"; // Text en negreta
    private static final String RESET = "\033[0m"; // Reset format

    // Filesiles
    private static final File personsFile = new File("FichPersona.dat");
    private static final File employeesFile = new File("FitxerEmpleats.dat");
    private static final File departmentsFile = new File("FitxerDepartaments.dat");
    private static final File prodJsonFile = new File("products1.json");
    // Later creation
    private static final File productsFile = new File("Products1.dat");

    public static void main(String[] args) throws IOException, ClassNotFoundException, ParserConfigurationException, SAXException, InterruptedException, ParseException {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        showMenu();
        int n = lector.nextInt();
        boolean exit = false;
            while (!exit) {
            switch (n) {
                case 1:
                    writePersons();
                    break;
                case 2:
                    readPersons();
                    break;
                case 3:
                    writeEmployees();
                    break;
                case 4:
                    writeDepartments();
                    break;
                case 5:
                    readEmployees();
                    break;
                case 6:
                    readDepartments();
                    break;
                case 7:
                    products1JsonToBinary();
                    break;
                case 8:
                    writeProducts1();
                    break;
                case 0:
                    exit = true;
                    return;
                default:
                    System.out.println("Opció invàlida. Torna a introduir-ne una altra:");
                    break;
            }
            promptEnterKey();
            showMenu();
            n = lector.nextInt();
        }
}

    // Obliga a presionar ENTER para avanzar.
    private static void promptEnterKey() throws InterruptedException {
        System.out.println("\nPrem \"ENTER\" per a continuar...");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    //MENU
    private static void showMenu() {
        System.out.println("\n\n\n\n\n");
        //Exericics
        System.out.println(BOLD+"FITXERS XML:"+RESET);
        System.out.println();
        //1
        System.out.println(BOLD+"EXERCICI 1:"+RESET);
        System.out.println("\t1.- Write 'FichPersona.dat' into "+BOLD+"'Personas.xml'"+RESET);
        System.out.println("\t2.- Read "+BOLD+"'Personas.xml"+RESET);
        System.out.println();
        //2
        System.out.println(BOLD+"EXERCICI 2:"+RESET);
        System.out.println("\t3.- Write 'FitxerEmpleats.dat' into "+BOLD+"'Empleats.xml'"+RESET);
        System.out.println("\t4.- Write 'FitxerDepartaments.dat' into "+BOLD+"'Departaments.xml'"+RESET);
        System.out.println();
        //3
        System.out.println(BOLD+"EXERCICI 3:"+RESET);
        System.out.println("\t5.- Read "+BOLD+"'Empleats.xml"+RESET);
        System.out.println("\t6.- Read "+BOLD+"'Departaments.xml"+RESET);
        System.out.println();
        //4
        System.out.println(BOLD+"EXERCICI 4:"+RESET);
        System.out.println("\t7.- Transform 'products1.json' into "+BOLD+"'Products1.dat'"+RESET);
        //5
        System.out.println();
        System.out.println(BOLD+"EXERCICI 5:"+RESET);
        System.out.println("\t8.- Write 'Products1.dat' into "+BOLD+"'Prodcuts1.xml'"+RESET);
        System.out.println();
        //Exit
        System.out.println("0.- SORTIR");
    }

    // =============================================================================
    // EXERCICI 1 | Write 'FichPersona.dat' into 'Personas.xml'; Read 'Personas.xml'
    // =============================================================================

    // Mètode per convertir 'FichPersona.dat' en un arxiu XML -> 'Personas.xml'
    public static void writePersons() throws IOException, ClassNotFoundException {
        //conecta el flujo de bytes al flujo de datos
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(personsFile));
        System.out.println("Comienza el proceso de creación del fichero " + personsFile.getName() + " a XML ...");

        //Creamos un objeto Lista de Personas
        ListaPersonas listaper = new ListaPersonas();

        try {
            while (true) { //lectura del fichero
                //leer una Persona
                Persona persona= (Persona) dataIS.readObject();
                listaper.add(persona); //añdir persona a la lista
            }
        }catch (EOFException eo) {}

        dataIS.close();  //cerrar stream de entrada

        try {
            XStream xstream = new XStream();

            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaPersonasMunicipio", ListaPersonas.class);
            xstream.alias("DatosPersona", Persona.class);

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection(ListaPersonas.class, "lista");

            //Insertar los objetos en el XML
            xstream.toXML(listaper, new FileOutputStream("Personas.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Mètode per llegir l'arxiu XML 'Personas.xml'
    public static void readPersons() throws ParserConfigurationException, SAXException {
        try {
            File file = new File("Personas.xml"); // El archivo a leer
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); // Nueva instancia de DocumentBuilderFactory
            DocumentBuilder db = dbf.newDocumentBuilder();  // Inicializamos nuevo documento
            Document document = db.parse(file); // Introducimos el arhivo parseado al documento
            document.getDocumentElement().normalize(); // Normalizamos el documento

            System.out.println("Root Element : " + document.getDocumentElement().getNodeName()); // Elemento raíz
            NodeList nList = document.getElementsByTagName("DatosPersona"); // Todos los siguientes elementos con el mismo tag "DatosPersona"
            System.out.println("----------------------------");

            // Por cada elemento "Datos Persona"
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element : " + nNode.getNodeName()); // Nodo padre de la lista. ("DatosPersona")
                if (nNode.getNodeType() == Node.ELEMENT_NODE) { // Si es un elemento de "DatosPersona"
                    Element eElement = (Element) nNode; // Pasamos el nodo casteado a elemento
                    System.out.println("Nombre : " + eElement.getElementsByTagName("nombre").item(0).getTextContent()); // Elemento con tag "nombre"
                    System.out.println("Edad : " + eElement.getElementsByTagName("edad").item(0).getTextContent()); // Elemento con tag "edad"
                }
            }
        }
        catch(IOException e) {
            System.out.println(e);
        }
    }

    // ===========================================================================================================
    // EXERCICI 2 | Write 'FitxerEmpleats.dat' & 'FitxerDepartaments.dat' into 'Empleats.xml' & 'Departaments.xml'
    // ===========================================================================================================

    // Mètode per covertir 'FitxerEmpleats.dat' en un arxiu XML -> 'Empleats.xml'
    public static void writeEmployees() throws IOException, ClassNotFoundException {
        // Obrim un fluxe de dades a l'arxiu binari 'FitxerEmpleats.dat'
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(employeesFile));
        System.out.println("Starting XML creation process of " + employeesFile.getName() + " ...");

        // Llista per guardar els empleats de 'FitxerEmpleats.dat'
        ListaEmpleats empList = new ListaEmpleats();

        try {
            while (true) { // Examinem l'arxiu
                // Llegir empleat
                Empleat e = (Empleat) dataIS.readObject();
                empList.add(e); // Afegim l'empleat a la llista
            }
        } catch (EOFException eo) {}

        dataIS.close();

        try {
            XStream xstream = new XStream();

            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaEmpleados", ListaEmpleats.class);
            xstream.alias("DatosEmpleado", Empleat.class);

            //quitar etiqueta lista (atributo de la claseListaEmpleados)
            xstream.addImplicitCollection(ListaEmpleats.class, "lista");

            //Insertar los objetos en el XML
            xstream.toXML(empList, new FileOutputStream("Empleats.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Mètode per covertir 'FitxerDepartaments.dat' en un arxiu XML -> 'Departaments.xml'
    public static void writeDepartments() throws IOException, ClassNotFoundException {
        // Obrim un fluxe de dades a l'arxiu binari 'FitxerDepartaments.dat'
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(departmentsFile));
        System.out.println("Starting XML creation process of " + departmentsFile.getName() + " ...");

        // Llista per guardar els departaments de 'FitxerDepataments.dat'
        ListaDepartaments depList = new ListaDepartaments();

        try {
            while (true) { // Examinem l'arxiu
                // Llegir departament
                Departament d = (Departament) dataIS.readObject();
                depList.add(d); // Afegim el departament a la llista
            }
        } catch (EOFException eo) {}

        dataIS.close();

        try {
            XStream xstream = new XStream();

            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaDepartamentos", ListaDepartaments.class);
            xstream.alias("DatosDepartamento", Departament.class);

            //quitar etiqueta lista (atributo de la claseListaEmpleados)
            xstream.addImplicitCollection(ListaDepartaments.class, "lista");

            //Insertar los objetos en el XML
            xstream.toXML(depList, new FileOutputStream("Departaments.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    // =====================================================
    // EXERCICI 3 | Read 'Empleats.xml' & 'Departaments.xml'
    // =====================================================

    // Mètode per llegir l'arxiu XML 'Empleats.xml'
    public static void readEmployees() throws ParserConfigurationException, SAXException {
        try {
            File file = new File("Empleats.xml"); // El archivo a leer
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); // Nueva instancia de DocumentBuilderFactory
            DocumentBuilder db = dbf.newDocumentBuilder();  // Inicializamos nuevo documento
            Document document = db.parse(file); // Introducimos el arhivo parseado al documento
            document.getDocumentElement().normalize(); // Normalizamos el documento

            System.out.println("Root Element : " + document.getDocumentElement().getNodeName()); // Elemento raíz
            NodeList nList = document.getElementsByTagName("DatosEmpleado"); // Todos los siguientes elementos con el mismo tag "DatosEmpleado"
            System.out.println("----------------------------");

            // Por cada elemento "Datos Empelado"
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element : " + nNode.getNodeName()); // Nodo padre de la lista. ("DatosEmpleado")
                if (nNode.getNodeType() == Node.ELEMENT_NODE) { // Si es un elemento de "DatosEmpleado"
                    Element eElement = (Element) nNode; // Pasamos el nodo casteado a elemento
                    System.out.println("ID : " + eElement.getElementsByTagName("id").item(0).getTextContent()); // Elemento con tag "id"
                    System.out.println("Apellido : " + eElement.getElementsByTagName("sureName").item(0).getTextContent()); // Elemento con tag "sureName"
                    System.out.println("Departamento : " + eElement.getElementsByTagName("department").item(0).getTextContent()); // Elemento con tag "department"
                    System.out.println("Salario : " + eElement.getElementsByTagName("salary").item(0).getTextContent()); // Elemento con tag "salary"
                }
            }
        }
        catch(IOException e) {
            System.out.println(e);
        }
    }
    // Mètode per llegir l'arxiu XML 'Departaments.xml'
    public static void readDepartments() throws ParserConfigurationException, SAXException {
        try {
            File file = new File("Departaments.xml"); // El archivo a leer
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); // Nueva instancia de DocumentBuilderFactory
            DocumentBuilder db = dbf.newDocumentBuilder();  // Inicializamos nuevo documento
            Document document = db.parse(file); // Introducimos el arhivo parseado al documento
            document.getDocumentElement().normalize(); // Normalizamos el documento

            System.out.println("Root Element : " + document.getDocumentElement().getNodeName()); // Elemento raíz
            NodeList nList = document.getElementsByTagName("DatosDepartamento"); // Todos los siguientes elementos con el mismo tag "DatosDepartamento"
            System.out.println("----------------------------");

            // Por cada elemento "Datos Empelado"
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element : " + nNode.getNodeName()); // Nodo padre de la lista. ("DatosDepartamento")
                if (nNode.getNodeType() == Node.ELEMENT_NODE) { // Si es un elemento de "DatosDepartamento"
                    Element eElement = (Element) nNode; // Pasamos el nodo casteado a elemento
                    System.out.println("ID : " + eElement.getElementsByTagName("id").item(0).getTextContent()); // Elemento con tag "id"
                    System.out.println("Nombre : " + eElement.getElementsByTagName("name").item(0).getTextContent()); // Elemento con tag "name"
                    System.out.println("Localidad : " + eElement.getElementsByTagName("location").item(0).getTextContent()); // Elemento con tag "location"
                }
            }
        }
        catch(IOException e) {
            System.out.println(e);
        }
    }

    // ==========================================================
    // EXERCICI 4 | Transform 'products1.json' to 'Products1.dat'
    // ==========================================================

    // Mètode per transformar l'arxiu JSON 'products1.json' en un arxiu binari 'Products1.dat'
    public static void products1JsonToBinary() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONArray a = (JSONArray) parser.parse(new FileReader(prodJsonFile));

        ListaProductes prodList = new ListaProductes();
        File products1Dat = new File("Products1.dat");
        ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(products1Dat)); // Flux de sortida per inserir dades sobre el fitxer

        for (Object o : a)
        {
            JSONObject product = (JSONObject) o;

            String name = (String) product.get("name");
            Long price = (Long) product.get("price");
            Long stock = (Long) product.get("stock");
            String picture = (String) product.get("picture");
            JSONArray products = (JSONArray) product.get("categories");

            List<Category1> catList = new ArrayList<>();
            for (Object ob : products) {
                String str = ob.toString();
                catList.add(new Category1(str));
            }

            Product1 p1 = new Product1(name, price, stock, picture, catList);

            prodList.add(p1);

        }

        for (Product1 p : prodList.getProductList()) {
            dataOS.writeObject(p);
        }

        dataOS.close();
    }

    // =======================================================
    // EXERCICI 5 | Write 'Products1.dat0 into 'Products1.xml'
    // =======================================================

    // Mètode per covertir 'Products1.dat' en un arxiu XML -> 'Products1.xml
    public static void writeProducts1() throws IOException, ClassNotFoundException {
        //conecta el flujo de bytes al flujo de datos
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(productsFile));
        System.out.println("Comienza el proceso de creación del fichero " + productsFile.getName() + " a XML ...");

        //Creamos un objeto Lista de Personas
        ListaProductes prodList = new ListaProductes();

        try {
            while (true) { //lectura del fichero
                //leer una Persona
                Product1 p = (Product1) dataIS.readObject();
                prodList.add(p); //añdir producto a la lista
            }
        }catch (EOFException eo) {}

        dataIS.close();  //cerrar stream de entrada

        try {
            XStream xstream = new XStream();

            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaProdcutos", ListaProductes.class);
            xstream.alias("DatosProducto", Product1.class);

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection(ListaProductes.class, "lista");

            //Insertar los objetos en el XML
            xstream.toXML(prodList, new FileOutputStream("Prodcuts1.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
