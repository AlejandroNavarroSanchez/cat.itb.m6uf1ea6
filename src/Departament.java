import java.io.Serializable;

public class Departament implements Serializable {
    private int id;
    private String name;
    private String location;

    /**
     * Constructor for Departaments. Creates a new Departament.
     * @param id Department's ID
     * @param name Department's name
     * @param location Department's location
     */
    public Departament(int id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }
    // Getters
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getLocation() {
        return location;
    }
    // Setters
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return String.format("Departament:\n\t→ ID: %d\n\t→ Nom: %s\n\t→ Localitat: %s", id, name, location);
    }
}
