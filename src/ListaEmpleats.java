import java.util.ArrayList;
import java.util.List;

public class ListaEmpleats {
    private List<Empleat> lista = new ArrayList<Empleat>();

    /**
     * Constructor for ListaEmpleats, creates a list of {@link Empleat}
     */
    public ListaEmpleats(){
    }

    public void add(Empleat e) {
        lista.add(e);
    }

    public List<Empleat> getEmployeeList() {
        return lista;
    }
}
